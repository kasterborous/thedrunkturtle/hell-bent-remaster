-- Hellbent Remaster

local T = {}
T.Base = "base"
T.Name = "Hell Bent (Remaster)"
T.ID = "rehellbent"

T.EnableClassicDoors = false

T.Versions = {
	randomize = true,
	allow_custom = true,
	randomize_custom = true,

	main = {
		classic_doors_id = "rehellbentcl",
		double_doors_id = "rehellbent",
	},
	other = {
		{
			name = "Sidrat Capsule",
			id = "rehellbentsidratr",
		},
	}
}

T.Interior = {
	RequireLightOverride = true,
	Model = "models/artixc/hellbent/interior.mdl",
	IdleSound = {
		{
			path = "artixc/hellbent/interior.wav",
			volume = 1
		},
	},
	LightOverride = {
		basebrightness = 0.05,
		nopowerbrightness = 0.02
	},
	Light={
		color=Color(255,237,157),
		pos=Vector(0,0,53),
		brightness=0.03

},
	Lights={
		{
			color=Color(199,179,255),
			pos=Vector(118.811, 191.859, 25.107),
			brightness=0.1
		},
		{
			color=Color(199,179,255),
			pos=Vector(86.435, 246.352, 23.748),
			brightness=0.1
		}
	},
	Lamps = {
		main = {
			color = Color(255, 255, 255),
			texture = "effects/flashlight/square",
			fov = 179,
			distance = 300,
			brightness = 0.2,
			pos = Vector(0.605, 0.634, 180.937),
			ang = Angle(90, 90, 0),
		},
		blue1 = {
			color = Color(181, 232, 255),
			texture = "models/artixc/hellbent/effect/hellbentglow",
			fov = 150,
			distance = 300,
			brightness = 5,
			pos = Vector(-202.76, 65.452, 41.166),
			ang = Angle(180, -30, 0),
		},
		blue2 = {
			color = Color(181, 232, 255),
			texture = "models/artixc/hellbent/effect/hellbentglow",
			fov = 150,
			distance = 300,
			brightness = 5,
			pos = Vector(-202.76, 65.452, 80.7),
			ang = Angle(180, -30, 0),
		},
		vent1 = {
			color = Color(181, 232, 255),
			texture = "effects/flashlight/soft",
			fov = 30,
			distance = 300,
			brightness = 0.1,
			pos = Vector(-125.392, 74.381, 130.7),
			ang = Angle(90, 90, 0),
			shadows = true
		},
		vent2 = {
			color = Color(181, 232, 255),
			texture = "effects/flashlight/soft",
			fov = 30,
			distance = 300,
			brightness = 0.1,
			pos = Vector(-171.487, -6.088, 130.7),
			ang = Angle(90, 90, 0),
			shadows = true
		},
		leftspotlight = {
			color = Color(255, 255, 255),
			texture = "effects/flashlight/soft",
			fov = 43.636363983154,
			distance = 300,
			brightness = 0.22727273404598,
			pos = Vector(-195, -46, 109.7),
			ang = Angle(90, -119.1, 180),
			shadows = false
		},
		rightspotlight = {
			color = Color(255, 255, 255),
			texture = "effects/flashlight/soft",
			fov = 43.636363983154,
			distance = 300,
			brightness = 0.22727273404598,
			pos = Vector(-102, 114.2, 109.7),
			ang = Angle(90, -119.1, 180),	
			shadows = false
		},
		pillarlight = {
			color = Color(255, 255, 255),
			texture = "effects/flashlight/soft",
			fov = 83.636360168457,
			distance = 300,
			brightness = 0.22727273404598,
			pos = Vector(64.8, 222.7, 115.8),
			ang = Angle(90, 127, 180),
			shadows = false
		},
		console = {
			color = Color(255, 255, 255),
			texture = "effects/flashlight/soft",
			fov = 107,
			distance = 300,
			brightness = 0.4,
			pos = Vector(-0.75, 0, 112.5),
			ang = Angle(85.4, 21, 76.8),
			shadows = false,
			nopower = true,

			off = {
				brightness = 0.2
			},
			warn = {
				color = Color(255, 88, 88),
			},
			off_warn = {
				color = Color(255, 88, 88),
			},
		},
		doubledoor = {
			color = Color(255, 255, 255),
			texture = "effects/flashlight/soft",
			fov = 83.636360168457,
			distance = 300,
			brightness = 0.22727273404598,
			pos = Vector(-12.21484375, 271.25634765625, 125.2958984375),
			ang = Angle(57.852638244629, 90, -180),
			shadows = false,
			nopower = true,

			off = {
				brightness = 0.2
			}
		},
	},
	Portal={
		pos=Vector(-12.525,325.1,44.45),
		ang=Angle(0,-90,0),
		width=35,
		height=88
	},
	Fallback={
		pos=Vector(0,80,5),
		ang=Angle(0,90,0),
	},
	Sounds = {
		Damage = {
			Crash = "jeredek/tardis/damage_collision.wav",
			BigCrash = "jeredek/tardis/damage_bigcollision.wav",
			Explosion = "jeredek/tardis/damage_explode.wav",
			Death = "jeredek/tardis/damage_death.wav",
			Artron = "p00gie/tardis/force_artron.wav",
		},
		Teleport = {}, -- uses exterior sounds if not specified
		Power = {
			On = "drmatt/tardis/power_on.wav",
			Off = "artixc/hellbent/powerdown.wav"
		},
		SequenceOK = "drmatt/tardis/seq_ok.wav",
		SequenceFail = "drmatt/tardis/seq_bad.wav",
		Cloister = "drmatt/tardis/cloisterbell_loop.wav",
		Lock = "drmatt/tardis/lock_int.wav",
	},
	ScreensEnabled = false,
	Screens = {
		{
			pos = Vector(-130.869, 9.353, 103.171),
			ang = Angle(0, 60, 90),
			width = 405,
			height = 250,
			visgui_rows = 2,
			power_off_black = false
		}
	},
	Sequences = "default_sequences",
	Parts = {
		door = {
			model = "models/artixc/hellbent/hbintdoors.mdl",
			posoffset = Vector(10.8,0,-53.6),
			angoffset = Angle(0,180,0)
		},
		hbfault = true,
		hbmaindoors = true,
		hbsidedoors = true,
		hbroundel = true,
		hbconsole = true,
		hbdoorwall = true,
		hbmonitor = true,
		hbrotor = true,
		hblightcovers = true,
		hblightbulbs = true,
		hbmetersfresnels = true,
		hbconsoleflap = true,
		hbdoubledoorwall = true,
		hbdevnote = true,
		hbglass = false, --broken for some reason
		hbbiglever1 = {pos = Vector(-9.311, -32.5, 37.225), ang = Angle(0, 0, 0), },
		hbbiglever2 = {pos = Vector(-2.14, -32.5, 37.225), ang = Angle(0, 0, 0), },
		hbbiglever3 = {pos = Vector(28.392, -15.799, 37.225), ang = Angle(0, 60, 0), },
		hbsmallred1 = {pos = Vector(4.902, -29.777, 42.151), ang = Angle(0, 0, 0), },
		hbsmallred2 = {pos = Vector(6.8, -29.777, 42.151), ang = Angle(0, 0, 0), },
		hbsmallred3 = {pos = Vector(8.67, -29.777, 42.151), ang = Angle(0, 0, 0), },
		hbsmallred4 = {pos = Vector(12.45, -29.777, 42.151), ang = Angle(0, 0, 0), },
		hbsmallred5 = {pos = Vector(19.027, -14.794, 44.06), ang = Angle(0, 60, 0), },
		hbsmallred6 = {pos = Vector(19.973, -13.142, 44.06), ang = Angle(0, 60, 0), },
		hbsmallred7 = {pos = Vector(22.804, -8.235, 44.06), ang = Angle(0, 60, 0), },
		hbsmallred8 = {pos = Vector(-18.596, 14.585, 44.24), ang = Angle(0, -120, 0), },
		hbsmallred9 = {pos = Vector(-28.678, 20.386, 40.449), ang = Angle(0, -120, 0), },
		hbsmallred10 = {pos = Vector(-25.412, -3.897, 44.08), ang = Angle(0, -60, 0), },
		hbsmallyellow1 = {pos = Vector(10.55, -29.777, 42.151), ang = Angle(0, 0, 0), },
		hbsmallyellow2 = {pos = Vector(14.27, -29.777, 42.151), ang = Angle(0, 0, 0), },
		hbsmallyellow3 = {pos = Vector(20.905, -11.501, 44.06), ang = Angle(0, 60, 0), },
		hbsmallyellow4 = {pos = Vector(21.84, -9.859, 44.06), ang = Angle(0, 60, 0), },
		hbsmallyellow5 = {pos = Vector(-23.598, 17.442, 42.386), ang = Angle(0, -120, 0), },
		hbsmallyellow6 = {pos = Vector(-30.4, 17.41, 40.441), ang = Angle(0, -120, 0), },
		hbsmallswitch1 = {pos = Vector(-5.65, -33.985, 40.747), ang = Angle(0, 0, 0), },
		hbsmallswitch2 = {pos = Vector(-37.614, 11.784, 39.287), ang = Angle(0, -120, 0), },
		hbsmallswitch3 = {pos = Vector(-32.18, -15.754, 40.25), ang = Angle(0, -60, 0), },
		hbsmallswitch4 = {pos = Vector(-32.936, -14.479, 40.25), ang = Angle(0, -60, 0), },
		hbsmallswitch5 = {pos = Vector(-33.63, -13.139, 40.25), ang = Angle(0, -60, 0), },
		hbsmallswitch6 = {pos = Vector(-33.661, -16.557, 39.68), ang = Angle(0, -60, 0), },
		hbsmallswitch7 = {pos = Vector(-34.488, -15.266, 39.68), ang = Angle(0, -60, 0), },
		hbsmallswitch8 = {pos = Vector(-35.244, -14.06, 39.68), ang = Angle(0, -60, 0), },
		hbsmallswitch9 = {pos = Vector(0.578, 38.425, 39.343), ang = Angle(0, -180, 0), },
		hbsmallswitch10 = {pos = Vector(2.987, 38.425, 39.343), ang = Angle(0, -180, 0), },
		hbsmallswitch11 = {pos = Vector(6, 38.425, 39.343), ang = Angle(0, -180, 0), },
		hbsmallswitch12 = {pos = Vector(7.7, 38.425, 39.343), ang = Angle(0, -180, 0), },
		hbsmallswitch13 = {pos = Vector(28.34, 21.829, 40.342), ang = Angle(0, 120, 0), },
		hbsmallswitch14 = {pos = Vector(27.39, 23.561, 40.342), ang = Angle(0, 120, 0), },
		hbsmallswitch15 = {pos = Vector(26.387, 25.428, 40.342), ang = Angle(0, 120, 0), },
		hbflipswitch1 = {pos = Vector(11.789, 38.422, 39.346), ang = Angle(0, 0, 0), },
		hbflipswitch2 = {pos = Vector(-3.63, 38.422, 39.346), ang = Angle(0, 0, 0), },
		hbflipswitch3 = {pos = Vector(-6.681, 38.422, 39.346), ang = Angle(0, 0, 0), },
		hbflipswitch4 = {pos = Vector(24.738, 26.521, 40.58), ang = Angle(0, -60, 0), },
		hbflipswitch5 = {pos = Vector(26, 27.254, 40.026), ang = Angle(0, -60, 0), },
		hbflipswitch6 = {pos = Vector(29.722, 18.036, 40.413), ang = Angle(0, -60, 0), },
		hbflipswitch7 = {pos = Vector(31.045, 18.812, 40.058), ang = Angle(0, -60, 0), },
		hblflipswitch1 = {pos = Vector(-18.849, 26.208, 42.202), ang = Angle(0, 0, 0), },
		hblflipswitch2 = {pos = Vector(29.407, 20.188, 40.261), ang = Angle(0, -120, 0), },
		hbyellowswitch = {pos = Vector(22.388, 4.711, 44.587), ang = Angle(0, 0, 0), },
		hbblackdial1 = {pos = Vector(-35.412, -12.068, 39.907), ang = Angle(0, 0, 0), },
		hbblackdial2 = {pos = Vector(33.48, 9.681, 41.398), ang = Angle(0, 180, 0), },
		hbkeys1 = {pos = Vector(5.754, -36.5, 39.85), ang = Angle(0, 0, 0), },
		hbkeys2 = {pos = Vector(-22.074, -28.35, 40.881), ang = Angle(0, 0, 0), },
		hbkeys3 = {pos = Vector(-15.432, -18.009, 44.375), ang = Angle(0, 0, 0), },
		hbkeys4 = {pos = Vector(-38.019, 5.458, 40.105), ang = Angle(0, 0, 0), },
		hbkeys5 = {pos = Vector(37.526, 9.956, 39.478), ang = Angle(0, 0, 0), },
		hbdest = {pos = Vector(0, 29.597, 42.108), ang = Angle(0, 0, 0), },
		hbcirclever1 = {pos = Vector(-21.805, -12.664, 43.445), ang = Angle(0, -60, 0), },
		hbcirclever2 = {pos = Vector(-37.647, -5.311, 40.3), ang = Angle(0, -60, 0), },
		hbcirclever3 = {pos = Vector(-41.193, -3.006, 39.7), ang = Angle(0, -60, 0), },
		hbcirclever4 = {pos = Vector(-23.145, 11.588, 43.4), ang = Angle(0, -120, 0), },
		hbcirclever5 = {pos = Vector(-25.083, 7.929, 43.4), ang = Angle(0, -120, 0), },
		hbcirclever6 = {pos = Vector(-21.535, 30.085, 40.736), ang = Angle(0, -120, 0), },
		hbcirclever7 = {pos = Vector(-25.412, 32.654, 39.269), ang = Angle(0, -120, 0), },
		hbcirclever8 = {pos = Vector(31.784, -11.419, 41.), ang = Angle(0, 60, 0), },
		hbcirclever9 = {pos = Vector(26.218, -21.197, 41.), ang = Angle(0, 60, 0), },
	},
	Controls = {
		hbbiglever1 = "teleport",
		hbbiglever2 = "handbrake",
		hbbiglever3 = "power",
		hbsmallred8 = "teleport",
		hbsmallred9 = "flight",
		hbsmallyellow5 = "float",
		hbsmallswitch2 = "door",
		hbsmallswitch13 = "door",
		hblflipswitch2 = "doorlock",
		hbflipswitch5 = "isomorphic",
		hbyellowswitch = "hads",
		hbdest = "destination",
		hbsmallswitch10 = "toggle_screens",
		hbflipswitch1 = "cloak",
		hblflipswitch1 = "doorlock",
		hbkeys1 = "coordinates",
		hbsmallswitch1 = "vortex_flight",
		hbsmallswitch3 = "toggle_screens",
		hbcirclever1 = "spin_cycle",
		hbcirclever2 = "cloak",
		hbkeys3 = "virtualconsole",
		hbkeys2 = "thirdperson",
		hbcirclever6 = "engine_release",
		hbcirclever7 = "physlock",
		hbcirclever8 = "repair",
		hbcirclever9 = "redecorate",
	},
	Tips = {},
	-- Interior.Tips are deprecated; should be deleted when the extensions update and
	-- replace with Interior.CustomTips, Interior.PartTips and Interior.TipSettings
	TipSettings = {
		view_range_min = 40,
		view_range_max = 60,
	},
	CustomTips = {
		--{ text = "Example", pos = Vector(0, 0, 0) },
	},
	PartTips = {
		hbbiglever1 = {pos = Vector(-9.311, -32.5, 40.225), down = true},
		hbbiglever2 = {pos = Vector(-2.14, -32.5, 40.225), down = true},
		hbbiglever3 = {pos = Vector(28.392, -15.799, 40.225), down = true},
		hbsmallswitch13 = {pos = Vector(28.383, 21.876, 40.479), down = true},
		hblflipswitch2 = {pos = Vector(29.486, 20.302, 40.455), down = false},
		hbflipswitch5 = {pos = Vector(26.063, 27.321, 40.373), down = true, right = true},
		hbyellowswitch = {pos = Vector(22.544, 4.921, 45.039), down = true, right = true},
		hbdest = {pos = Vector(0, 29.762, 42.898), down = true, right = true},
		hbsmallswitch10 = {pos = Vector(3.033, 38.471, 39.404), down = true, right = true},
		hbflipswitch1 = {pos = Vector(11.818, 38.561, 39.85), down = true, right = true},
		hbsmallswitch2 = {pos = Vector(-37.611, 11.789, 39.429), down = true, right = true},
		hbsmallred8 = {pos = Vector(-20.795, 15.786, 44.09), down = true, right = false},
		hbsmallyellow5 = {pos = Vector(-25.746, 18.646, 42.297), down = true, right = false},
		hbsmallred9 = {pos = Vector(-30.905, 21.679, 40.403), down = true, right = false},
		hblflipswitch1 = {pos = Vector(-19.027, 26.34, 42.325), down = true, right = false},
		hbkeys1 = {pos = Vector(5.807, -37.712, 39.961), down = true, right = false},
		hbcirclever1 = {pos = Vector(-22.258, -12.932, 44.267), down = true, right = false},
		hbsmallswitch3 = {pos = Vector(-32.18, -15.754, 40.25), down = true, right = true},
		hbcirclever2 = {pos = Vector(-37.647, -5.311, 40.3), down = true, right = true},
		hbkeys3 = {pos = Vector(-15.432, -18.009, 44.375), down = true, right = true},
		hbkeys2 = {pos = Vector(-22.074, -28.35, 40.881), down = true, right = true},
		hbcirclever6 = {pos = Vector(-21.535, 30.085, 40.736), down = true, right = true},
		hbcirclever7 = {pos = Vector(-25.412, 32.654, 39.269), down = true, right = false},
		hbcirclever8 = {pos = Vector(31.784, -11.419, 41.), down = true, right = true},
		hbcirclever9 = {pos = Vector(26.218, -21.197, 41.), down = true, right = false},
	},
	Seats = {
		{
			pos = Vector(130, -96, -30),
			ang = Angle(0, 40, 0)
		},
		{
			pos = Vector(125, 55, -30),
			ang = Angle(0, 135, 0)
		}
	},
	BreakdownEffectPos = Vector(0, 0, 40),
}

T.Exterior={
	Model="models/artixc/hellbent/exterior.mdl",
	Mass=2000,
	DoorAnimationTime = 0.65,
	Portal={
		pos=Vector(19.5,0,52.25),
		ang=Angle(0,0,0),
		width=32,
		height=87,
		thickness = 25,
		inverted = true,
	},
	Fallback={
		pos=Vector(35,0,5.5),
		ang=Angle(0,0,0)
	},
	Light={
		enabled=false,
	},
	Sounds={
		Teleport={
			demat="artixc/hellbent/demat.wav",
			mat="artixc/hellbent/mat.wav"
		},
		Lock="doctorwho1200/baker/lock.wav", --TEMP 
		Door={
			enabled=true,
			open="artixc/hellbent/doorext_open.wav",
			close="artixc/hellbent/doorext_close.wav",
		},
		FlightLoop="artixc/hellbent/flight_loop.wav"
	},
	Parts={
			door={
				model="models/artixc/hellbent/hbcapdoor.mdl",
				posoffset=Vector(-19.5,0,-52.2),
				angoffset=Angle(0,0,0),
				},
		vortex={
			model="models/doctorwho1200/toyota/2014timevortex.mdl",--TEMP 
			pos=Vector(0,0,50),
			ang=Angle(0,0,0),
			scale=10
		}
	},
	Teleport = {
		SequenceSpeed = 0.5,
		SequenceSpeedFast = 0.935,
		DematSequence = {
			255,
			200,
			150,
			100,
			70,
			50,
			20,
			0
		},
		MatSequence = {
			0,
			20,
			50,
			100,
			150,
			180,
			255
		}
	}
}

T.Interior.TextureSets = {
	normal = {
		prefix = "models/artixc/hellbent/",
		{ "hbfault", 0, "hbrneon" },
		{ "hbfault", 2, "hbflneon" },
		{ "hbmaindoors", 1, "hbrneon" },
		{ "hbsidedoors", 1, "hbrneon" },
		{ "intdoor", 1, "hbrneon" },

		{ "hbmonitor", 1, "hbscreen" },

		{ "hblightbulbs", 0, "lightbulbs" },

		{ "hbmetersfresnels", 2, "fresnels" },
	},
	flight = {
		prefix = "models/artixc/hellbent/",
		{ "hbfault", 0, "hbrneonflight" },
		{ "hbfault", 2, "hbflneon" },
		{ "hbmaindoors", 1, "hbrneonflight" },
		{ "hbsidedoors", 1, "hbrneonflight" },
		{ "intdoor", 1, "hbrneonflight" },

		{ "hbmonitor", 1, "hbscreeninflight" },

		{ "hblightbulbs", 0, "lightbulbsflight" },
	},
	off = {
		prefix = "models/artixc/hellbent/",
		{ "hbfault", 0, "hbrneonoff" },
		{ "hbfault", 2, "hbrneonoff" },
		{ "hbmaindoors", 1, "hbrneonoff" },
		{ "hbsidedoors", 1, "hbrneonoff" },
		{ "intdoor", 1, "hbrneonoff" },

		{ "hbmonitor", 1, "black" },

		{ "hblightbulbs", 0, "lightbulbsoff" },

		{ "hbmetersfresnels", 2, "fresnelsoff" },
	},
}

local TEXTURE_UPDATE_DATA_IDS = {
	["power-state"] = true,
	["health-warning"] = true,
	["teleport"] = true,
	["vortex"] = true,
	["flight"] = true,
}

T.CustomHooks = {
	travel_textures = {
		exthooks = {
			["DataChanged"] = true,
		},
		func = function(ext, int, data_id, data_value)
			if not TEXTURE_UPDATE_DATA_IDS[data_id] then return end

			prefix = "models/artixc/hellbent/"

			local power = ext:GetData("power-state")
			local warning = ext:GetData("health-warning")
			local teleport = ext:GetData("teleport")
			local flight = ext:GetData("flight")
			local vortex = ext:GetData("vortex")

			if not power then
				int:ApplyTextureSet("off")
			else
				if flight or teleport or vortex then
					int:ApplyTextureSet(warning and "warning_flight" or "flight")
				else
					int:ApplyTextureSet(warning and "warning" or "normal")
				end
			end
		end,
	},
}

T.CustomSettings = {
	dark = {
		text = "Enable Dark Mode",
		value_type = "bool",
		value = false,
	},
}

T.Templates = {
	exterior_ttcapsule_type55 = { override = true, fail = function() ErrorNoHalt("Failed to add tt_sidrat default exterior") end, },
	hellbent_dark = {
		override = true,
		condition = function(id, ply, ent)
			return TARDIS:GetCustomSetting(id, "dark", ply)
		end,
	},
}

TARDIS:AddInterior(T)


