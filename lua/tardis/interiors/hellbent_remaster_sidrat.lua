--Hell Bent TARDIS Sidrat

local T={}
T.Base="rehellbent"
T.Name="HellBent Remaster Sidrat"
T.ID="rehellbentsidratr"
T.EnableClassicDoors = true

T.IsVersionOf = "rehellbent"

T.Interior={
	Portal = {
		pos = Vector(132.962, 37.101, 43),
		ang = Angle(0, -180, 0),
		width = 150,
		height = 200
	},
	Fallback={
		pos = Vector(115.385, 37.311, 0),
		ang = Angle(0, 90, 0),
	},
	Sounds={
		Door={
			enabled=true,
			open = "artixc/hellbent/door.wav",
			close = "artixc/hellbent/door.wav",
		},
	},
	Parts={
		hbmaindoors=false,
		intdoor = {
			model="models/artixc/hellbent/maindoors.mdl",
			ang = Angle(0, 0, 0),
		},
		door={
			posoffset = Vector(door_offset, 1.25, -44.75),
		},
	},
	Controls = {},
	IntDoorAnimationTime = 2,
}

T.Templates = {
	exterior_sidrat = { override = true, fail = function() ErrorNoHalt("Failed to add tt_sidrat default exterior") end, },
}

T.CustomHooks = {
	classic_doors_intdoor = {
		exthooks = {},
		inthooks = {
			["ShouldDrawPart"] = true,
		},
		func = function(ext, int, part)
			if SERVER then return end
			if not IsValid(int) then return end
			if wp.drawing and wp.drawingent == int.portals.exterior
				and int:GetPart("hbdoorwall") == part
			then
				return false
			end
		end,
	},
}

TARDIS:AddInterior(T)

--		pos = Vector(132.962, 37.101, 52.5),