TARDIS:AddInteriorTemplate("hellbent_dark", {
    Interior = {
        Lamps = {
            main = {
                color = Color(255, 255, 255),
                texture = "effects/flashlight/square",
                fov = 179,
                distance = 300,
                brightness = 0,
                pos = Vector(0, 40, 150),
                ang = Angle(90, 0, 90),
                shadows = false,

                warn = {
                    color = Color(255, 88, 88),
                },

                },
            },
    },
})