local PART={}
PART.ID = "hbbiglever3"
PART.Name = "HellBent large slider"

PART.Model = "models/artixc/hellbent/controls/hbbiglever.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 2.5

PART.SoundOff = "artixc/hellbent/leveroff.wav"
PART.SoundOn = "artixc/hellbent/leveron.wav"


TARDIS:AddPart(PART)