local PART={}
PART.ID = "hbrotor"
PART.Name = "HellBent Rotor"
PART.Model = "models/artixc/hellbent/rotor.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true
PART.Animate = true

if CLIENT then
	function PART:Initialize()
		self.rotor={
			pos = 0,
			mode = 1,
		}
		self.rotorspin={
			pos = 0,
			mode = 1,
		}
	end

	function PART:Think()
		local power=self:GetData("power-state")
		local active = self:GetData("flight") or self:GetData("teleport") or self:GetData("vortex")

		if power then
			if self.rotorspin.pos == 0 then
				self.rotorspin.pos = 1
			elseif self.rotorspin.pos == 1 then
				self.rotorspin.pos = 0
			end

			self.rotorspin.pos = math.Approach( self.rotorspin.pos, self.rotorspin.mode, FrameTime()*0.04 )

			if self.rotor.pos > 0 or active then
				if self.rotor.pos == 0 then
					self.rotor.pos = 1
				elseif self.rotor.pos == 1 and active then
					self.rotor.pos = 0
				end

				self.rotor.pos=math.Approach( self.rotor.pos, self.rotor.mode, FrameTime()*0.5 )
			end
		end

		self:SetPoseParameter( "rotorspin", self.rotorspin.pos )
		self:SetPoseParameter( "rotor", self.rotor.pos )

	end
end


TARDIS:AddPart(PART)