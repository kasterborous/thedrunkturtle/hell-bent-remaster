local PART={}
PART.ID = "hbmaindoors"
PART.Name = "HellBent Main Doors"
PART.Model = "models/artixc/hellbent/maindoors.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.6
PART.ShouldTakeDamage = true

PART.Sound = "artixc/hellbent/door.wav"
PART.SoundPos = Vector(0,0,0)

if SERVER then
	function PART:Use()
		self:SetCollide(self:GetOn())
	end

	function PART:Toggle( bEnable, ply )
		sound.Play(self.Sound, self:LocalToWorld(self.SoundPos))
		self:SetOn(bEnable)
		self:SetCollide(not bEnable)
	end
end

TARDIS:AddPart(PART)
