local PART={}
PART.ID = "hbsmallyellow2"
PART.Name = "HellBent small yellow switch"

PART.Model = "models/artixc/hellbent/controls/hbsmallyellow.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 2.5

PART.Sound = "artixc/hellbent/smalllever.wav"


TARDIS:AddPart(PART)