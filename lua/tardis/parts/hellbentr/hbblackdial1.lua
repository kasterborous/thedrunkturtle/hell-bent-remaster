local PART={}
PART.ID = "hbblackdial1"
PART.Name = "HellBent black dial"

PART.Model = "models/artixc/hellbent/controls/hbblackdial.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 5

PART.Sound = "artixc/hellbent/wheel_switch.wav"


TARDIS:AddPart(PART)