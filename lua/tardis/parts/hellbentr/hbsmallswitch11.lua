local PART={}
PART.ID = "hbsmallswitch9"
PART.Name = "HellBent small switch"

PART.Model = "models/artixc/hellbent/controls/hbsmallswitch.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 5

PART.Sound = "artixc/hellbent/switch.wav"


TARDIS:AddPart(PART)