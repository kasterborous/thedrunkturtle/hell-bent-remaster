local PART={}
PART.ID = "hbkeys2"
PART.Name = "HellBent piano keys"

PART.Model = "models/artixc/hellbent/controls/hbkeys2.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 5

PART.Sound = "artixc/hellbent/switch.wav"


TARDIS:AddPart(PART)