local PART={}
PART.ID = "hbyellowswitch"
PART.Name = "HellBent yellow switch"

PART.Model = "models/artixc/hellbent/controls/hbyellowswitch.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 5

PART.Sound = "artixc/hellbent/switch.wav"


TARDIS:AddPart(PART)