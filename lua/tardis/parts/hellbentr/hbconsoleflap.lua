local PART={}
PART.ID = "hbconsoleflap"
PART.Name = "HellBent Console Flap"

PART.Model = "models/artixc/hellbent/hbconsoleflap.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 2.5

PART.Sound = "artixc/hellbent/wheel_switch.wav"


TARDIS:AddPart(PART)