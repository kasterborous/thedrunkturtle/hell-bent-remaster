local PART={}
PART.ID = "hbcirclever2"
PART.Name = "HellBent Circle Lever"

PART.Model = "models/artixc/hellbent/controls/hbcirclever.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 5

PART.Sound = "artixc/hellbent/telepathic.wav"


TARDIS:AddPart(PART)