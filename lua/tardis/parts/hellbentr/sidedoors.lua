local PART={}
PART.ID = "hbsidedoors"
PART.Name = "HellBent Side Doors"
PART.Model = "models/artixc/hellbent/sidedoors.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.9
PART.ShouldTakeDamage = true

PART.Sound = "artixc/hellbent/lqdoorsopen.wav"
PART.SoundPos = Vector(0,0,0)

if SERVER then
	function PART:Use()
		self:SetCollide(self:GetOn())
	end

	function PART:Toggle( bEnable, ply )
		sound.Play(self.Sound, self:LocalToWorld(self.SoundPos))
		self:SetOn(bEnable)
		self:SetCollide(not bEnable)
	end
end

TARDIS:AddPart(PART)
