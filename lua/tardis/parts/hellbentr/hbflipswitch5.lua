local PART={}
PART.ID = "hbflipswitch5"
PART.Name = "HellBent flip switch"

PART.Model = "models/artixc/hellbent/controls/hbflipswitch.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 5

PART.Sound = "artixc/hellbent/switch.wav"


TARDIS:AddPart(PART)