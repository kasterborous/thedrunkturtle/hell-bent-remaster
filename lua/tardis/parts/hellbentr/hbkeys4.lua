local PART={}
PART.ID = "hbkeys4"
PART.Name = "HellBent piano keys"

PART.Model = "models/artixc/hellbent/controls/hbkeys4.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 5

PART.Sound = "artixc/hellbent/switch.wav"


TARDIS:AddPart(PART)