local PART={}
PART.ID = "hbdest"
PART.Name = "HellBent Telepathic Circuits"

PART.Model = "models/artixc/hellbent/controls/hbdest.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 5

PART.Sound = "artixc/hellbent/telepathic.wav"


TARDIS:AddPart(PART)