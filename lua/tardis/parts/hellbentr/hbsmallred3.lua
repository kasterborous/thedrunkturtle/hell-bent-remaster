local PART={}
PART.ID = "hbsmallred3"
PART.Name = "HellBent small red switch"

PART.Model = "models/artixc/hellbent/controls/hbsmallred.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 2.5

PART.Sound = "artixc/hellbent/smalllever.wav"


TARDIS:AddPart(PART)