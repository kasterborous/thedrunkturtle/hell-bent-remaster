local PART={}
PART.ID = "hblflipswitch2"
PART.Name = "HellBent larger flip switch"

PART.Model = "models/artixc/hellbent/controls/hblflipswitch.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 5

PART.Sound = "artixc/hellbent/switch.wav"


TARDIS:AddPart(PART)