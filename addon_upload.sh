#! /bin/bash

gma_file="tardis_extension_hellbent_remaster.gma"
addon_id="648567023"

[[ -z $* ]] && echo "Changelog not specified! Aborting." && exit 1
[[ ! -f ./$gma_file ]] && echo "File $gma_file does not exist! Aborting." && exit 2

gmpublish update -id $addon_id -addon $gma_file -changes "$*"
[[ $? == 0 ]] && rm $gma_file